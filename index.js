//2
console.log("Hello World");

//3 & 4
let num1 = parseInt(prompt("Please provide a number:"));
let num2 = parseInt(prompt("Please provide another number:"));

let total = num1 + num2;

let arithmeticOperation =function (num1,num2){
	if(total<10){ 
		console.warn(`The sum of the two numbers is ${total}`);
	} else if(total >= 10 && total <=20){
		let diff =  num1 - num2;
		alert(`The difference of the two numbers is ${diff}`);
	} else if(total>20 && total <=29){
		let product = num1 * num2;
		alert(`The product of the two numbers is ${product}`);
	} else {
		let quotient = num1 / num2;
		alert(`The quotient of the two numbers is ${quotient}`);
	}
};

//5
let name = prompt("What is your name?");
let age = parseInt(prompt("How old are you?"));

if (name.length === 0 || name == null || age.length === 0 || age == null){
	alert("Are you a time traveler?");
} else {
	alert(`Hello ${name}. Your age is ${age}`);
};

//#6
function isLegalAge(age){
	if (age >= 18) {
		alert("You are now allowed to party.");
	}
	else {
		alert("You are not allowed here."); 
	}
};
isLegalAge(age);

// 7
switch(true){
	case (age>=18) && (age <21):
		console.log("You are now allowed to party.");
		break;
	case (age>=21) && (age <65):
		console.log("You are now part of the adult society.");
		break;
	case (age>=65):
		console.log("We thank you for your contribution to society.");
		break;
	default:
		console.log("Are you sure you're not an alien?");
		break;
};
//8

let tryAge = "stringinput";
try {
	if(isNaN(tryAge)){
		throw Error("Input is not a number.");
		}
	else {
		console.log("try: Success");
	}
	}
catch(err){
	console.warn("catch: "+err);
	}
finally {
	alert("finally:"+isLegalAge(parseInt(tryAge)));
};

/*******STRETCH GOALS********/

//1
let dayToday = prompt("What is the day today?").toLowerCase();
switch (dayToday) {
			case "monday":
				alert(`Today is ${dayToday}, Wear Black`);
				break;
			case "tuesday":
				alert(`Today is ${dayToday}, Wear Green`);
				break;
			case "wednesday":
				alert(`Today is ${dayToday}, Wear Yellow`);
				break;
			case "thursday":
				alert(`Today is ${dayToday}, Wear Red`);
				break;
			case "friday":
				alert(`Today is ${dayToday}, Wear Violet`);
				break;
			case "saturday":
				alert(`Today is ${dayToday}, Wear Blue`);
				break;
			case "sunday":
				alert(`Today is ${dayToday}, Wear White`);
				break;
			default:
				alert(`Invalid input. Enter a valid day of the week.`);	
				break;

};

//2
let num3;

function oddEvenChecker(num3){
	if((typeof num3) === "number"){
		if(num3 % 2 === 0){
			console.log("The number is even.");
		}
		else{
			console.log("The number is odd.");
		}
	}
	else{
		alert("Invalid input.");
	}
};
oddEvenChecker(99);

//3
let num4;

function budgetChecker(num4){
	if((typeof num4) === "number"){
		if(num4 > 40000){
			console.log("You are over the budget.");
		}
		else{
			console.log("You have resources left.");
		}
	}
	else{
		alert("Invalid input.");
	}
};

budgetChecker(4000);